package me.hope.d2d.dsl

import me.hope.d2d.core.LinkedChannel
import me.hope.d2d.core.PendingChannel
import net.dv8tion.jda.core.entities.MessageChannel

fun linkedChannel(channel: MessageChannel, init: LinkedChannel.() -> Unit): LinkedChannel {
    val linked = LinkedChannel(channel)
    linked.apply(init)
    return linked
}

fun pendingChannel(channel: MessageChannel, init: PendingChannel.() -> Unit):PendingChannel {
    val pending = PendingChannel(channel)
    pending.apply(init)
    return pending

}