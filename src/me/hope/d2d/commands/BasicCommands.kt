package me.hope.d2d.commands


import me.aberrantfox.hotbot.arguments.HexColourArg
import me.aberrantfox.hotbot.arguments.MultipleArg
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.arg
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.ChoiceArg
import me.aberrantfox.kjdautils.internal.command.arguments.TextChannelArg
import me.hope.d2d.Utils.asLinkedChannel
import me.hope.d2d.Utils.asPendingChannel
import me.hope.d2d.Utils.getInfo
import me.hope.d2d.core.Enums.MessageType
import me.hope.d2d.core.LinkedChannel
import net.dv8tion.jda.core.entities.TextChannel
import java.awt.Color


@CommandSet("Basic")
fun basicCommands() = commands {
    command("id") {
        expect(arg(TextChannelArg, true) { it.channel })
        execute {
            val channel = it.args.component1() as TextChannel
            it.respond("${channel.name} -> ${channel.id}")
        }
    }
    command("link") {
        expect(arg(TextChannelArg, true) { it.channel }, arg(MultipleArg(TextChannelArg)))
        execute {
            val issuer = it.args.component1() as TextChannel
            val targetChannels = it.args.component2() as List<TextChannel>
            if (targetChannels.contains(issuer)) it.respond("You can't link the channel to itself.")

            else targetChannels.forEach { target ->
                val embed = embed {
                    val guildName = it.guild?.name
                    val guildLogo = it.guild?.iconUrl
                    val issuerChannelName = issuer.name
                    val targetChannelName = target.name
                    title("Request from $guildName")
                    setFooter("from $guildName", guildLogo)
                    color(Color.YELLOW)
                    description("$guildName wants to link their channel #$issuerChannelName to your channel #$targetChannelName")
                }
                target.sendMessage(embed).queue {
                    it.addReaction("✅").queue()
                    it.addReaction("❌").queue()
                    issuer.asPendingChannel().setRequest(it, target)
                }

            }
        }
    }



    command("unlink") {
        expect(arg(TextChannelArg, true) { it.channel }, arg(MultipleArg(TextChannelArg), false))
        execute {
            val issuer = it.args.component1() as TextChannel
            val targetChannels = it.args.component2() as List<TextChannel>
            targetChannels.forEach { target -> LinkedChannel(issuer).unlink(target) }
            it.respond("Unlinked.")
        }
    }
    command("color") {
        expect(arg(TextChannelArg, true) { it.channel }, arg(HexColourArg, false))
        execute {
            val channel = it.args.component1() as TextChannel
            val colorText = it.args.component2() as String
            channel.getInfo().setColor(colorText)
            it.respond("Color set to $colorText")
        }
    }

    command("type") {
        expect(arg(TextChannelArg, true) { it.channel }, arg(ChoiceArg("Type", "normal", "embed"), false))
        execute {
            val channel = it.args.component1() as TextChannel
            val choice = it.args.component2() as String
            val value = MessageType.valueOf(choice.toUpperCase())
            channel.getInfo().setType(value)
            it.respond("Color set to $choice")
        }
    }

    command("list") {
        expect(arg(TextChannelArg, true) { it.channel })
        execute {
            val channel = it.args.component1() as TextChannel
            val builder = StringBuilder()
            val sortedList = channel.asLinkedChannel().getTextChannels().sortedBy { it.guild.name }
            sortedList.forEach {
                val guild = it.guild
                val id = it.id
                val name = it.name
                builder.appendln("$guild -> #$name($id)")

            }

            val embed = embed {
                setColor(Color.ORANGE)
                title("Linked Channels to #${channel.name}")
                setDescription(builder.toString())
                setFooter("Count: ${sortedList.size}",null)
            }

            it.respond(embed)

        }
    }

}
