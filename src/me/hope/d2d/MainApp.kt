package me.hope.d2d


import me.aberrantfox.kjdautils.api.startBot
import me.aberrantfox.kjdautils.extensions.jda.toMember
import me.aberrantfox.kjdautils.internal.command.Fail
import me.aberrantfox.kjdautils.internal.command.Pass
import me.hope.d2d.core.Tables.ChannelsInfo
import me.hope.d2d.core.Tables.LinkedChannels
import me.hope.d2d.core.Tables.PendingChannels
import net.dv8tion.jda.core.Permission
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction


fun main(args: Array<String>) {

    Database.connect("jdbc:postgresql://localhost/d2d", "org.postgresql.Driver", "", "")
    transaction { SchemaUtils.create(LinkedChannels, PendingChannels,ChannelsInfo) }
    val token = ""
    startBot(token) {

        configure {
            prefix = "+"
            commandPath = "me.hope.d2d.commands"
            listenerPath = "me.hope.d2d.listeners"
        }
        registerCommandPreconditions({
            if (it.author.toMember(it.guild!!).hasPermission(Permission.ADMINISTRATOR)) Pass
            else Fail("You are not an Admin in this guild.")
        })


    }

}