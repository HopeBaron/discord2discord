package me.hope.d2d.Utils

import me.hope.d2d.core.LinkedChannel
import me.hope.d2d.core.PendingChannel
import me.hope.d2d.core.Tables.ChannelsInfo
import me.hope.d2d.core.Tables.LinkedChannels
import me.hope.d2d.core.Tables.PendingChannels
import net.dv8tion.jda.core.entities.MessageChannel
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

fun PendingChannels.searchTable(channelId: String) = transaction {
    select {
        issuerChannel.eq(channelId) or targetChannel.eq(channelId)
    }
}

fun PendingChannels.searchById(requestId: String) = transaction {
    select {
        id.eq(requestId)
    }
}

fun LinkedChannels.searchTable(channelId: String) = transaction {
    select {
        firstChannel.eq(channelId) or secondChannel.eq(channelId)
    }

}
fun ChannelsInfo.searchTable(channelId:String) = transaction {
    select { id.eq(channelId) }
}

fun MessageChannel.asPendingChannel() = PendingChannel(this)


fun MessageChannel.asLinkedChannel() = LinkedChannel(this)
fun MessageChannel.getInfo() = this.asLinkedChannel().getInfo()
fun getConst(value: Any) {

}



