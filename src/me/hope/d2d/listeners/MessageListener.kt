package me.hope.d2d.listeners

import com.google.common.eventbus.Subscribe
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.extensions.jda.fullName
import me.hope.d2d.core.Enums.MessageType
import me.hope.d2d.dsl.linkedChannel
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent

class MessageListener {
    @Subscribe
    fun onMessage(event: GuildMessageReceivedEvent) {
        val eventAuthor = event.author
        if (!eventAuthor.isBot) {
            val content = event.message.contentDisplay
            linkedChannel(event.channel) {
                getLinkedTextChannels().forEach {
                    val info = it.getInfo()
                    if (info.getType() == MessageType.EMBED) {
                        val color = info.getColor()
                        val message = embed {
                            val guildName = event.guild.name
                            val channelName = event.channel.name
                            val guildLogo = event.guild.iconUrl
                            title("message from $guildName")
                            setAuthor(event.author.name, null, event.author.effectiveAvatarUrl)
                            color(color)
                            description(content)
                            setFooter("#$channelName", guildLogo)
                        }
                        it.messageChannel().sendMessage(message).queue()
                    } else {
                        val appended = StringBuilder()
                                .appendln("**${eventAuthor.fullName()}**")
                                .appendln(content)
                        it.messageChannel().sendMessage(appended).queue()
                    }
                }
            }
        }
    }
}