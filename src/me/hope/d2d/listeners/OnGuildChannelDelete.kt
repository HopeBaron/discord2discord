package me.hope.d2d.listeners

import com.google.common.eventbus.Subscribe
import me.hope.d2d.Utils.asLinkedChannel
import me.hope.d2d.Utils.asPendingChannel
import me.hope.d2d.Utils.getInfo
import net.dv8tion.jda.core.events.channel.text.TextChannelDeleteEvent

class OnGuildChannelDelete {
    @Subscribe
    fun onDelete(event:TextChannelDeleteEvent) {
        event.channel.asLinkedChannel().unlinkAll()
        event.channel.asPendingChannel().deleteAllRequests()
        event.channel.getInfo().delete()
    }
}