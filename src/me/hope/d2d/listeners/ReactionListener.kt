package me.hope.d2d.listeners

import com.google.common.eventbus.Subscribe
import me.aberrantfox.kjdautils.api.dsl.embed
import me.hope.d2d.core.Enums.Status
import me.hope.d2d.core.PendingChannel
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.MessageEmbed
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent
import java.awt.Color

class ReactionListener {
    @Subscribe
    fun onReact(event: GuildMessageReactionAddEvent) {
        if (!event.user.isBot && event.member.hasPermission(Permission.ADMINISTRATOR)) {
            val reaction = event.reactionEmote.name
            val pendingChannel = PendingChannel(event.channel)
            val request = pendingChannel.getRequest(event.messageId)
            if (request != null) {
                val guildName = event.guild.name
                val issuerName = request.getIssuerChannel().name
                val targetName = request.getTargetChannel().name
                val embedMessage: MessageEmbed
                if (request.getStatus() == Status.PENDING) {
                    if (reaction == "✅") {
                        request.approve()

                        embedMessage = embed {
                            color(Color.GREEN)
                            title("Request Accepted")
                            description("$guildName accepted your request to link #$issuerName with #$targetName")
                        }
                        request.getIssuerChannel().sendMessage(embedMessage).queue()
                    } else if (reaction == "❌") {
                        request.decline()
                        embedMessage = embed {
                            color(Color.RED)
                            title("Request Rejected")
                            description("$guildName rejected your request to link #$issuerName with #$targetName")
                        }
                        request.getIssuerChannel().sendMessage(embedMessage).queue()
                    }

                }
            }
        }
    }
}

