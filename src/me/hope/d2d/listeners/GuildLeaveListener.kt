package me.hope.d2d.listeners

import com.google.common.eventbus.Subscribe
import me.hope.d2d.Utils.asLinkedChannel
import me.hope.d2d.Utils.asPendingChannel
import me.hope.d2d.Utils.getInfo
import net.dv8tion.jda.core.events.guild.GuildLeaveEvent

class GuildLeaveListener {
    @Subscribe
    fun onLeave(event: GuildLeaveEvent) {
        val guild = event.guild
        guild.textChannels.forEach {
            it.asLinkedChannel().unlinkAll()
            it.asPendingChannel().deleteAllRequests()
            it.getInfo().delete()
        }
    }
}