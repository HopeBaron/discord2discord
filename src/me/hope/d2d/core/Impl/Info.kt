package me.hope.d2d.core.Impl

import me.hope.d2d.core.Enums.MessageType
import java.awt.Color

interface Info {
    fun setType(t: MessageType)
    fun getType(): MessageType
    fun setColor(c:String)
    fun getColor(): Color
    fun delete()
}