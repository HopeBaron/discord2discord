package me.hope.d2d.core.Impl

import me.hope.d2d.core.ChannelInfo
import me.hope.d2d.core.PendingChannel
import me.hope.d2d.core.Request
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.TextChannel

interface Pending {
    fun getRequestingChannels(): List<PendingChannel>
    fun getRequests(): List<Request>
    fun deleteAllRequests()
    fun deleteRequestsOfGuild(guild:Guild)
    fun messageChannel(): MessageChannel
    fun setRequest(message: Message, channel: TextChannel)
    fun id(): String
    fun getInfo():ChannelInfo
}