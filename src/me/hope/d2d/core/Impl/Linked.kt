package me.hope.d2d.core.Impl

import me.hope.d2d.core.ChannelInfo
import me.hope.d2d.core.LinkedChannel
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.User

interface Linked {
    fun getTextChannels(): List<MessageChannel>
    fun getLinkedTextChannels(): List<LinkedChannel>
    fun messageChannel(): MessageChannel
    fun unlink(linkedChannel: LinkedChannel)
    fun unlink(messageChannel: MessageChannel)
    fun filterHistory(user: User, amount: Int): List<Message>
    fun unlinkGuild(guild: Guild)
    fun unlinkAll()
    fun getInfo(): ChannelInfo

}