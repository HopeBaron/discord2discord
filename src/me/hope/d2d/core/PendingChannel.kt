package me.hope.d2d.core

import me.hope.d2d.Utils.asPendingChannel
import me.hope.d2d.Utils.searchTable
import me.hope.d2d.core.Impl.Pending
import me.hope.d2d.core.Tables.PendingChannels
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.TextChannel
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction

class PendingChannel(private val messageChannel: MessageChannel) : Pending {


    private val channelId = messageChannel.id
    private val jda = messageChannel.jda

    override fun messageChannel() = messageChannel
    override fun id() = channelId

    override fun getRequestingChannels(): List<PendingChannel> {
        val requestingChannels = ArrayList<PendingChannel>()

        transaction {
            with(PendingChannels) {
                searchTable(channelId).forEach {
                    val pendingChannelId = if (it[targetChannel] == channelId) it[issuerChannel]
                    else it[targetChannel]

                    val pendingMessageChannel = jda.getTextChannelById(pendingChannelId)
                    requestingChannels.add(pendingMessageChannel.asPendingChannel())
                }
            }
        }
        return requestingChannels
    }

    override fun getRequests(): List<Request> {
        val requests = ArrayList<Request>()
        transaction {
            with(PendingChannels) {
                searchTable(channelId).forEach {
                    val target = it[targetChannel]
                    if (!isIssuerTo(target)) {
                        val requestId = it[id]
                        val message = messageChannel.getMessageById(requestId).complete()
                        val pendingChannel = jda.getTextChannelById(it[issuerChannel])
                        requests.add(Request(pendingChannel, messageChannel, message))

                    }
                }
            }
        }

        return requests
    }
    private fun isIssuerTo(id: String): Boolean {
        var state = false
        return transaction {
            with(PendingChannels) {
                searchTable(channelId).forEach {
                    if (it[issuerChannel] == channelId) state = true
                }
                return@transaction state

            }
        }

    }

    override fun setRequest(message: Message, channel: TextChannel) {
        transaction {
            PendingChannels.apply {
                insert {
                    it[id] = message.id
                    it[issuerChannel] = this@PendingChannel.channelId
                    it[targetChannel] = channel.id
                    it[status] = 0
                }
            }
        }
    }

    override fun deleteAllRequests() {
        getRequests().forEach {
            it.delete()
        }
    }

    override fun deleteRequestsOfGuild(guild: Guild) {
        guild.textChannels.forEach { textChannel ->
            textChannel.asPendingChannel().getRequests().forEach {
                it.delete()
            }

        }
    }

    override fun getInfo() = ChannelInfo(messageChannel)
    fun asLinkedChannel() = LinkedChannel(messageChannel)
    fun getRequest(id: String) = getRequests().firstOrNull { it.getId() == id }

}