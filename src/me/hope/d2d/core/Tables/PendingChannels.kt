package me.hope.d2d.core.Tables

import org.jetbrains.exposed.sql.Table

object PendingChannels : Table() {
    val id = text("id").primaryKey()
    val issuerChannel = text("issuerchannel")
    val targetChannel = text("targetChannel")
    val status = integer("status")
}