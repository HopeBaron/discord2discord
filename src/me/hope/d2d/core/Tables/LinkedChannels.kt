package me.hope.d2d.core.Tables

import org.jetbrains.exposed.sql.Table

object LinkedChannels : Table() {
    val id = text("id").primaryKey()
    val firstChannel = text("firstchannel")
    val secondChannel = text("secondchannel")
}


