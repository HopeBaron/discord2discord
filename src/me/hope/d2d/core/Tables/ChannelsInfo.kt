package me.hope.d2d.core.Tables

import org.jetbrains.exposed.sql.Table

object ChannelsInfo : Table() {
    val id = text("id").primaryKey()
    val color = text("color")
    val messageType = integer("messagetype")
}
