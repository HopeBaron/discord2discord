package me.hope.d2d.core

import me.hope.d2d.Utils.searchById
import me.hope.d2d.core.Enums.Status
import me.hope.d2d.core.Tables.LinkedChannels
import me.hope.d2d.core.Tables.PendingChannels
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.entities.MessageChannel
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update


class Request(private val issuerChannel: MessageChannel, private val targetChannel: MessageChannel, private val message: Message) {
    private val jda = issuerChannel.jda
    private val messageId = message.id
    fun message() = message
    fun approve() {
        transaction {
            updateStatus(Status.APPROVED)
            LinkedChannels.apply {
                insert {
                    it[id] = message.id
                    it[firstChannel] = this@Request.issuerChannel.id
                    it[secondChannel] = this@Request.targetChannel.id
                }
            }
        }

    }


    fun decline() = updateStatus(Status.DECLINED)
    fun suspend() = updateStatus(Status.SUSPENDED)
    fun getId() = message.id
    fun getStatus() =
            transaction {
                var value = 0
                with(PendingChannels) {
                    searchById(messageId).forEach {
                        value = it[status]
                    }

                }
                return@transaction Status.values()[value]
            }


    fun getIssuerChannel() = issuerChannel
    fun getTargetChannel() = targetChannel
    fun delete() {
        message.delete().queue()
        PendingChannels.apply {
            deleteWhere {
                id.eq(message.id)
            }
        }
    }

    private fun updateStatus(state: Status) {
        transaction {
            PendingChannels.apply {
                update({ id.eq(message.id) }, null, { it[status] = state.state })
            }
        }
    }


}

