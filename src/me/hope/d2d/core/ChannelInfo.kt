package me.hope.d2d.core

import me.hope.d2d.Utils.searchTable
import me.hope.d2d.core.Enums.MessageType
import me.hope.d2d.core.Impl.Info
import me.hope.d2d.core.Tables.ChannelsInfo
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.MessageChannel
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.awt.Color

 class ChannelInfo : Info {

    private val channel: MessageChannel
    private val jda: JDA
    private val channelId: String

    constructor(channel: MessageChannel) {
        this.channel = channel
        jda = channel.jda
        channelId = channel.id
        transaction {
            ChannelsInfo.apply {
                if (searchTable(channelId).empty()) {
                    insert {
                        it[id] = channelId
                        it[color] = "#FFFFFF"
                        it[messageType] = MessageType.EMBED.type
                    }
                }
            }
        }

    }

    constructor(channel: LinkedChannel) : this(channel.messageChannel())

    override fun setType(t: MessageType) {
        transaction {
            with(ChannelsInfo) {
                update({ id.eq(channelId) }, null, {
                    it[messageType] = t.type
                })

            }
        }
    }

    override fun setColor(c: String) {
        transaction {
            with(ChannelsInfo) {
                update({ id.eq(channelId) }, null, {
                    it[color] = c
                })
            }
        }
    }

    override fun getColor(): Color {
        return transaction {
            with(ChannelsInfo) {
                val searchTable = searchTable(channelId)
                val color = if (!searchTable.empty()) searchTable.first()[color] else "FFFFFF"
                return@transaction Color.decode(color)
            }
        }

    }


    override fun getType(): MessageType {
        return transaction {
            with(ChannelsInfo) {
                val searchTable = searchTable(channelId)
                val type = if (!searchTable.empty()) searchTable.first()[messageType] else 0
                return@transaction MessageType.values()[type]
            }
        }
    }

    override fun delete() {
        transaction {
            ChannelsInfo.deleteWhere {
                ChannelsInfo.id.eq(channelId)
            }
        }
    }


}