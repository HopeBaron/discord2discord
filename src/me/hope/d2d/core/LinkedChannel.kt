package me.hope.d2d.core

import me.hope.d2d.Utils.asLinkedChannel
import me.hope.d2d.Utils.searchTable
import me.hope.d2d.core.Impl.Linked
import me.hope.d2d.core.Tables.LinkedChannels
import net.dv8tion.jda.core.entities.*
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.transactions.transaction

class LinkedChannel(private val messageChannel: MessageChannel) : Linked {


    private val channelId = messageChannel.id
    private val jda = messageChannel.jda

    override fun getTextChannels(): List<TextChannel> {
        val linkedMessageChannels = ArrayList<TextChannel>()
        transaction {
            with(LinkedChannels) {
                searchTable(channelId).forEach {
                        val channelId = if (it[firstChannel] == channelId) it[secondChannel] else it[firstChannel]
                        val messageChannel = jda.getTextChannelById(channelId)
                        linkedMessageChannels.add(messageChannel)
                    }
                }
            }

        return linkedMessageChannels
    }


    override fun unlink(messageChannel: MessageChannel) {
        transaction {
            LinkedChannels.apply {
                deleteWhere {
                    (firstChannel.eq(channelId) and secondChannel.eq(messageChannel.id)) or
                            (secondChannel.eq(channelId) and firstChannel.eq(messageChannel.id))
                }
            }
        }
    }

    override fun unlink(linkedChannel: LinkedChannel) {
        val messageChannel = linkedChannel.messageChannel
        unlink(messageChannel)
    }

    override fun filterHistory(user: User, amount: Int): List<Message> {
        val messages = messageChannel.history.retrievePast(amount).complete()
        val sentByUser = ArrayList<Message>()
        for (message in messages) {
            if (message.author == user) sentByUser.add(message)
            else break
        }


        return sentByUser
    }


    override fun getLinkedTextChannels(): List<LinkedChannel> {
        val linkedTextChannels = ArrayList<LinkedChannel>()
        getTextChannels().forEach {
            linkedTextChannels.add(it.asLinkedChannel())
        }
        return linkedTextChannels
    }

    override fun unlinkGuild(guild: Guild) {
        guild.textChannels.forEach { unlink(it) }
    }

    override fun unlinkAll() {
        getLinkedTextChannels().forEach {
            unlink(it)
        }
    }


    override fun messageChannel() = messageChannel
    override fun getInfo(): ChannelInfo = ChannelInfo(messageChannel)
    fun asPendingChannel() = PendingChannel(messageChannel)


}




