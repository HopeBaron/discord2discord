package me.hope.d2d.core.Enums

enum class Status(val state:Int) {

    PENDING(0),
    APPROVED(1),
    DECLINED(2),
    SUSPENDED(3)
}