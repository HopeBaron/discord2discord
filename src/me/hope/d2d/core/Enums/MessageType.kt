package me.hope.d2d.core.Enums

enum class MessageType(val type:Int) {
    EMBED(0),
    NORMAL(1)
}